package com.adminportal.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adminportal.domain.Artwork;
import com.adminportal.repository.ArtworkRepository;
import com.adminportal.service.ArtworkService;

@Service
public class ArtworkServiceImpl implements ArtworkService{
	
	@Autowired
	private ArtworkRepository artworkRepository;
	
	public Artwork save(Artwork artwork) {
		return artworkRepository.save(artwork);
	}
	
	public List<Artwork> findAll() {
		return (List<Artwork>) artworkRepository.findAll();
	}
	
	public Artwork findOne(Long id) {
		return artworkRepository.findOne(id);
	}
}
