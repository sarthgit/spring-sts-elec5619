package com.adminportal.service;

import java.util.List;

import com.adminportal.domain.Artwork;

public interface ArtworkService {
	
	Artwork save(Artwork artwork);

	List<Artwork> findAll();
	
	Artwork findOne(Long id);
}
