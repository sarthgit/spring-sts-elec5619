package com.adminportal.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.adminportal.domain.Artwork;
import com.adminportal.service.ArtworkService;

@Controller
@RequestMapping("/artwork")
public class ArtworkController {

	@Autowired
	private ArtworkService artworkService;

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addartwork(Model model) {
		Artwork artwork = new Artwork();
		model.addAttribute("artwork", artwork);
		return "addartwork";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addartworkPost(@ModelAttribute("artwork") Artwork artwork, HttpServletRequest request) {
		artworkService.save(artwork);

		MultipartFile artworkImage = artwork.getArtworkImage();

		try {
			byte[] bytes = artworkImage.getBytes();
			String name = artwork.getId() + ".png";
			BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(new File("src/main/resources/static/image/artwork/" + name)));
			stream.write(bytes);
			stream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:artworkList";
	}
	
	@RequestMapping("/artworkInfo")
	public String artworkInfo(@RequestParam("id") Long id, Model model) {
		Artwork artwork = artworkService.findOne(id);
		model.addAttribute("artwork", artwork);
		
		return "artworkInfo";
	}
	
	@RequestMapping("/updateartwork")
	public String updateartwork(@RequestParam("id") Long id, Model model) {
		Artwork artwork = artworkService.findOne(id);
		model.addAttribute("artwork", artwork);
		
		return "updateartwork";
	}
	
	@RequestMapping(value="/updateartwork", method=RequestMethod.POST)
	public String updateartworkPost(@ModelAttribute("artwork") Artwork artwork, HttpServletRequest request) {
		artworkService.save(artwork);
		
		MultipartFile artworkImage = artwork.getArtworkImage();
		
		if(!artworkImage.isEmpty()) {
			try {
				byte[] bytes = artworkImage.getBytes();
				String name = artwork.getId() + ".png";
				
				Files.delete(Paths.get("src/main/resources/static/image/artwork/"+name));
				
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File("src/main/resources/static/image/artwork/" + name)));
				stream.write(bytes);
				stream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return "redirect:/artwork/artworkInfo?id="+artwork.getId();
	}
	
	@RequestMapping("/artworkList")
	public String artworkList(Model model) {
		List<Artwork> artworkList = artworkService.findAll();
		model.addAttribute("artworkList", artworkList);		
		return "artworkList";
		
	}

}
