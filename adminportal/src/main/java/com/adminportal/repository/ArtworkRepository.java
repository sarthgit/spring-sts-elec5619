package com.adminportal.repository;

import org.springframework.data.repository.CrudRepository;

import com.adminportal.domain.Artwork;

public interface ArtworkRepository extends CrudRepository<Artwork, Long>{

}
