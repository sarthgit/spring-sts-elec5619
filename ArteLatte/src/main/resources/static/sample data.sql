drop database ArteLattedb;
create database ArteLattedb;
use artelattedb;

INSERT INTO artwork (title,artist,description,our_price,list_price, in_stock_number,active,shipping_weight) VALUES ("towers","JANE","DA",11,12,3,true,10);
INSERT INTO artwork (title,artist,description,our_price,list_price, in_stock_number,active,shipping_weight) VALUES ("two","JANE","DA",11,12,3,true,10);
INSERT INTO artwork (title,artist,description,our_price,list_price, in_stock_number,active,shipping_weight) VALUES ("rings","JANE","DA",11,12,3,true,10);

INSERT INTO shopping_cart (grand_total,user_id) VALUES (0,1);


SELECT * FROM orders;