package com.ArteLatte.service;


import java.util.List;

import com.ArteLatte.domain.Artwork;
import com.ArteLatte.domain.CartItem;
import com.ArteLatte.domain.ShoppingCart;
import com.ArteLatte.domain.User;

public interface CartItemService {
	List<CartItem> findByShoppingCart(ShoppingCart shoppingCart);
	
	CartItem updateCartItem(CartItem cartItem);
	
	CartItem addArtworkToCartItem(Artwork artwork, User user, int qty);
	
	CartItem findById(Long id);
	
	void removeCartItem(CartItem cartItem);
	
	CartItem save(CartItem cartItem);
}
