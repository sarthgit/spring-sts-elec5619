package com.ArteLatte.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ArteLatte.domain.Artwork;
import com.ArteLatte.domain.ArtworkToCartItem;
import com.ArteLatte.domain.CartItem;
import com.ArteLatte.domain.ShoppingCart;
import com.ArteLatte.domain.User;
import com.ArteLatte.repository.ArtworkToCartItemRepository;
import com.ArteLatte.repository.CartItemRepository;
import com.ArteLatte.service.CartItemService;

@Service
public class CartItemServiceImpl implements CartItemService {
	
	@Autowired
	private CartItemRepository cartItemRepository;

	@Autowired
	private ArtworkToCartItemRepository artworkToCartItemRepository;
	
	@Override
	public List<CartItem> findByShoppingCart(ShoppingCart shoppingCart) {
		return cartItemRepository.findByShoppingCart(shoppingCart);
	}

	@Override
	public CartItem updateCartItem(CartItem cartItem) {
		BigDecimal bigDecimal = new BigDecimal(cartItem.getArtwork().getOurPrice()).multiply(new BigDecimal(cartItem.getQty()));
		
		bigDecimal = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
		cartItem.setSubtotal(bigDecimal);
		
		cartItemRepository.save(cartItem);
		
		return cartItem;
	}

	@Override
	public CartItem addArtworkToCartItem(Artwork artwork, User user, int qty) {
		List<CartItem> cartItemList = findByShoppingCart(user.getShoppingCart());

		for (CartItem cartItem : cartItemList) {
			if(artwork.getId() == cartItem.getArtwork().getId()) {
				cartItem.setQty(cartItem.getQty()+qty);
				cartItem.setSubtotal(new BigDecimal(artwork.getOurPrice()).multiply(new BigDecimal(qty)));
				cartItemRepository.save(cartItem);
				return cartItem;
			}
		}
		
		CartItem cartItem = new CartItem();
		cartItem.setShoppingCart(user.getShoppingCart());
		cartItem.setArtwork(artwork);
		
		cartItem.setQty(qty);
		cartItem.setSubtotal(new BigDecimal(artwork.getOurPrice()).multiply(new BigDecimal(qty)));
		cartItem = cartItemRepository.save(cartItem);

		ArtworkToCartItem artworkToCartItem = new ArtworkToCartItem();
		artworkToCartItem.setBook(artwork);
		artworkToCartItem.setCartItem(cartItem);
		artworkToCartItemRepository.save(artworkToCartItem);
		
		return cartItem;
	}

	@Override
	public CartItem findById(Long id) {
		return cartItemRepository.findOne(id);
	}

	@Override
	public void removeCartItem(CartItem cartItem) {
		cartItemRepository.delete(cartItem);

	}

	@Override
	public CartItem save(CartItem cartItem) {
		return cartItemRepository.save(cartItem);
	}

}
