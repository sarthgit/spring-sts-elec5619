package com.ArteLatte.service.impl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ArteLatte.domain.Artwork;
import com.ArteLatte.domain.CartItem;
import com.ArteLatte.repository.OrdersRepository;
import com.ArteLatte.service.CartItemService;

import com.ArteLatte.domain.Orders;
import com.ArteLatte.domain.ShoppingCart;
import com.ArteLatte.domain.User;
import com.ArteLatte.service.OrdersService;

@Service
public class OrdersServiceImpl implements OrdersService {
	@Autowired
	private OrdersRepository orderRepository;
	
	@Autowired
	private CartItemService cartItemService;
		
	@Override
	public Orders createOrder(ShoppingCart shoppingCart, User user) {
		Orders order = new Orders();
		
		List<CartItem> cartItemList = cartItemService.findByShoppingCart(shoppingCart);
		
		for(CartItem cartItem : cartItemList) {
			Artwork book = cartItem.getArtwork();
			cartItem.setOrder(order);
			book.setInStockNumber(book.getInStockNumber() - cartItem.getQty());
		}
		
		order.setCartItemList(cartItemList);
		order.setOrderTotal(shoppingCart.getGrandTotal());
		order.setUser(user);
		order = orderRepository.save(order);
		
		return order;
	}

	@Override
	public Orders findOne(Long id) {
		return orderRepository.findOne(id);
	}

}
