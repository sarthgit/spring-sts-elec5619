package com.ArteLatte.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ArteLatte.domain.Artwork;
import com.ArteLatte.repository.ArtworkRepository;
import com.ArteLatte.service.ArtworkService;

@Service
public class ArtworkServiceImpl implements ArtworkService{
	@Autowired
	private ArtworkRepository artworkRepository;
	
	public List<Artwork> findAll() {
		return (List<Artwork>) artworkRepository.findAll();
	}
	
	public Artwork findOne(Long id) {
		return artworkRepository.findOne(id);
	}

}
