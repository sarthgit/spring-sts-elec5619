package com.ArteLatte.service;

import java.util.Set;

import com.ArteLatte.domain.User;
import com.ArteLatte.domain.UserBilling;
import com.ArteLatte.domain.UserPayment;
import com.ArteLatte.domain.UserShipping;
import com.ArteLatte.domain.security.PasswordResetToken;
import com.ArteLatte.domain.security.UserRole;

public interface UserService {
	PasswordResetToken getPasswordResetToken(final String token);
	
	void createPasswordResetTokenForUser(final User user, final String token);
	
	User findByUsername(String username);
	
	User findByEmail (String email);
	
	User createUser(User user, Set<UserRole> userRoles) throws Exception;
	
	User save(User user);
	
	void updateUserBilling(UserBilling userBilling, UserPayment userPayment, User user);
	
	void updateUserShipping(UserShipping userShipping, User user);
	
	void setUserDefaultPayment(Long userPaymentId, User user);
	
	void setUserDefaultShipping(Long userShippingId, User user);
}
