package com.ArteLatte.service;

import com.ArteLatte.domain.UserShipping;

public interface UserShippingService {
	UserShipping findById(Long id);
	
	void removeById(Long id);
}
