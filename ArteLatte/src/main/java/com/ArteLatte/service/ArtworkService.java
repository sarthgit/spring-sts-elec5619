package com.ArteLatte.service;

import java.util.List;

import com.ArteLatte.domain.Artwork;

public interface ArtworkService {
	List<Artwork> findAll ();
	
	Artwork findOne(Long id);
}
