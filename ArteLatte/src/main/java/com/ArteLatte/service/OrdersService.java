package com.ArteLatte.service;

import com.ArteLatte.domain.Orders;
import com.ArteLatte.domain.ShoppingCart;
import com.ArteLatte.domain.User;

public interface OrdersService {
	Orders createOrder(ShoppingCart shoppingCart, User user);
	
	Orders findOne(Long id);
}
