package com.ArteLatte.service;

import com.ArteLatte.domain.UserPayment;

public interface UserPaymentService {
	UserPayment findById(Long id);
	
	void removeById(Long id);
}
