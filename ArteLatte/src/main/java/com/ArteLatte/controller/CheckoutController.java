package com.ArteLatte.controller;


import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ArteLatte.domain.Artwork;
import com.ArteLatte.domain.CartItem;
import com.ArteLatte.domain.ShoppingCart;
import com.ArteLatte.domain.User;
import com.ArteLatte.service.ArtworkService;
import com.ArteLatte.service.CartItemService;
import com.ArteLatte.service.OrdersService;
import com.ArteLatte.service.ShoppingCartService;
import com.ArteLatte.service.UserService;

@Controller
@RequestMapping("/shoppingCart")
public class CheckoutController {
	
	private static final Logger LOG = LoggerFactory.getLogger(CheckoutController.class);

	@Autowired
	private UserService userService;
	
	@Autowired
	private CartItemService cartItemService;
	
	@Autowired
	private ArtworkService artworkService;
	
	@Autowired
	private ShoppingCartService shoppingCartService;
	
	@RequestMapping("/cart")
	public String shoppingCart(Model model, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		ShoppingCart shoppingCart = user.getShoppingCart();
		
		if (shoppingCart == null) {
			shoppingCart = new ShoppingCart();
			shoppingCart.setUser(user);
		}
		
		LOG.info("--> CheckoutController here");
		
		List<CartItem> cartItemList = cartItemService.findByShoppingCart(shoppingCart);
		
		LOG.info("--> CheckoutController here 54");
		
		shoppingCartService.updateShoppingCart(shoppingCart);
		
		model.addAttribute("cartItemList", cartItemList);
		model.addAttribute("shoppingCart", shoppingCart);
		
		return "checkout";
	}
	
	@RequestMapping("/addItem")
	public String addItem(
			@ModelAttribute("artwork") Artwork artwork,
			@ModelAttribute("qty") String qty,
			Model model, Principal principal
			) {
		User user = userService.findByUsername(principal.getName());
		artwork = artworkService.findOne(artwork.getId());
		
		if (Integer.parseInt(qty) > artwork.getInStockNumber()) {
			model.addAttribute("notEnoughStock", true);
			return "forward:/artworkDetail?id="+artwork.getId();
		}
		
		CartItem cartItem = cartItemService.addArtworkToCartItem(artwork, user, Integer.parseInt(qty));
		model.addAttribute("addArtworkSuccess", true);
		
		return "forward:/artworkDetail?id="+artwork.getId();
	}
	
	@RequestMapping("/updateCartItem")
	public String updateShoppingCart(
			@ModelAttribute("id") Long cartItemId,
			@ModelAttribute("qty") int qty
			) {
		CartItem cartItem = cartItemService.findById(cartItemId);
		cartItem.setQty(qty);
		cartItemService.updateCartItem(cartItem);
		
		return "forward:/shoppingCart/cart";
	}
	
	@RequestMapping("/removeItem")
	public String removeItem(@RequestParam("id") Long id) {
		cartItemService.removeCartItem(cartItemService.findById(id));
		
		return "forward:/shoppingCart/cart";
	}
	
	
	
	@Autowired
	OrdersService orders;
	
	@RequestMapping("/checkout")
	public String checkout(Model model,
			Principal principal) {
		User user = userService.findByUsername(principal.getName());

		ShoppingCart shoppingCart = user.getShoppingCart();

		List<CartItem> cartItemList = cartItemService.findByShoppingCart(shoppingCart);
		
		shoppingCartService.updateShoppingCart(shoppingCart);
		
		
		orders.createOrder(shoppingCart, user);
		
		shoppingCartService.clearShoppingCart(shoppingCart);

		return "forward:/shoppingCart/cart";

	}

}
