package com.ArteLatte.repository;

import org.springframework.data.repository.CrudRepository;

import com.ArteLatte.domain.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
	Role findByname(String name);
}
