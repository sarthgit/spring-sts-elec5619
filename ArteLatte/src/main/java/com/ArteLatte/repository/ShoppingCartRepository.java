package com.ArteLatte.repository;

import org.springframework.data.repository.CrudRepository;

import com.ArteLatte.domain.ShoppingCart;

public interface ShoppingCartRepository  extends CrudRepository<ShoppingCart, Long> {

}
