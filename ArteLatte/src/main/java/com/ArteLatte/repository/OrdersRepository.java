package com.ArteLatte.repository;

import org.springframework.data.repository.CrudRepository;

import com.ArteLatte.domain.Orders;

public interface OrdersRepository extends CrudRepository<Orders, Long> {

}
