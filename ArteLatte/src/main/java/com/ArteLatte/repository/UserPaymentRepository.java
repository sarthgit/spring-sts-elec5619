package com.ArteLatte.repository;

import org.springframework.data.repository.CrudRepository;

import com.ArteLatte.domain.UserPayment;

public interface UserPaymentRepository extends CrudRepository<UserPayment, Long>{

}
