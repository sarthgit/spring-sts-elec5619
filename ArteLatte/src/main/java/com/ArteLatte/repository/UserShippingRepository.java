package com.ArteLatte.repository;

import org.springframework.data.repository.CrudRepository;

import com.ArteLatte.domain.UserShipping;

public interface UserShippingRepository extends CrudRepository<UserShipping, Long> {
	
	

}
