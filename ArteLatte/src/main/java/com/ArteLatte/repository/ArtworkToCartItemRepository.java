package com.ArteLatte.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.ArteLatte.domain.ArtworkToCartItem;
import com.ArteLatte.domain.CartItem;

@Transactional
public interface ArtworkToCartItemRepository extends CrudRepository<ArtworkToCartItem, Long> {
	void deleteByCartItem(CartItem cartItem);
}
