package com.ArteLatte.service;

import com.ArteLatte.domain.CartItemList;

public interface CartItemListService {
	CartItemList updateShoppingCart(CartItemList shoppingCart);

}
