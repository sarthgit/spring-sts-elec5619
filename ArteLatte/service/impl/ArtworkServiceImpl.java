package com.ArteLatte.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ArteLatte.domain.Artwork;
import com.ArteLatte.service.ArtworkService;
import com.ArteLatte.repository.ArtworkRepository;

@Service
public class ArtworkServiceImpl implements ArtworkService {

	@Autowired
	private ArtworkRepository artworkRepository;
	
	@Override
	public List<Artwork> findAll() {
		return (List<Artwork>) this.artworkRepository.findAll();
	}

	@Override
	public Artwork findOne(Long id) {
		return this.artworkRepository.findOne(id);
	}

}
