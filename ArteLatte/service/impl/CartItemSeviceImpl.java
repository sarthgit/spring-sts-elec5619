package com.ArteLatte.service.impl;

import java.util.List;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ArteLatte.domain.CartItem;
import com.ArteLatte.domain.CartItemList;
import com.ArteLatte.service.CartItemService;
import com.ArteLatte.domain.Artwork;
import com.ArteLatte.domain.User;
import com.ArteLatte.repository.CartItemRepository;

@Service
public class CartItemSeviceImpl implements CartItemService {

	@Autowired
	private CartItemRepository cartItemRepository;

	
	@Override
	public List<CartItem> findByShoppingCart(CartItemList shoppingCart) {
		return cartItemRepository.findByShoppingCart(shoppingCart);
	}

	@Override
	public CartItem updateCartItem(CartItem cartItem) {
		BigDecimal bigDecimal = new BigDecimal(cartItem.getArtwork().getPrice()).multiply(new BigDecimal(cartItem.getQty()));
		
		bigDecimal = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
		cartItem.setSubtotal(bigDecimal);
		
		cartItemRepository.save(cartItem);
		
		return cartItem;
	}

	@Override
	public CartItem findById(Long id) {
		return cartItemRepository.findOne(id);
	}

	@Override
	public void removeCartItem(CartItem cartItem) {
		cartItemRepository.delete(cartItem);
	}

	@Override
	public CartItem addBookToCartItem(Artwork artwork, User user, int qty) {
		List<CartItem> cartItemList = findByShoppingCart(user.getCartItemList());
		
		for (CartItem cartItem : cartItemList) {
			if(artwork.getId() == cartItem.getArtwork().getId()) {
				cartItem.setQty(cartItem.getQty()+qty);
				cartItem.setSubtotal(new BigDecimal(artwork.getPrice()).multiply(new BigDecimal(qty)));
				cartItemRepository.save(cartItem);
				return cartItem;
			}
		}
		
		CartItem cartItem = new CartItem();
		cartItem.setShoppingCart(user.getCartItemList());
		cartItem.setArtwork(artwork);
		
		cartItem.setQty(qty);
		cartItem.setSubtotal(new BigDecimal(artwork.getPrice()).multiply(new BigDecimal(qty)));
		cartItem = cartItemRepository.save(cartItem);
		
		return cartItem;
	}

}
