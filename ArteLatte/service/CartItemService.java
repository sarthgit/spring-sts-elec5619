package com.ArteLatte.service;

import java.util.List;
import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ArteLatte.domain.Artwork;
import com.ArteLatte.domain.CartItem;
import com.ArteLatte.domain.CartItemList;
import com.ArteLatte.domain.User;

@Service
public interface CartItemService {
	List<CartItem> findByShoppingCart(CartItemList shoppingCart);
	
	CartItem updateCartItem(CartItem cartItem);
	
	CartItem addBookToCartItem(Artwork artwork, User user, int qty);
	
	CartItem findById(Long id);
	
	void removeCartItem(CartItem cartItem);
}
