package com.ArteLatte.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import org.springframework.transaction.annotation.Transactional;

import com.ArteLatte.domain.CartItem;
import com.ArteLatte.domain.CartItemList;

@Transactional
public interface CartItemRepository extends CrudRepository<CartItem, Long> {
	List<CartItem> findByShoppingCart(CartItemList shoppingCart);
}
