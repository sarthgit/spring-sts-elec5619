package com.ArteLatte.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ArteLatte.domain.Artwork;
import com.ArteLatte.domain.CartItem;
import com.ArteLatte.domain.CartItemList;
import com.ArteLatte.domain.User;
import com.ArteLatte.service.ArtworkService;
import com.ArteLatte.service.CartItemService;
import com.ArteLatte.service.CartItemListService;
import com.ArteLatte.service.UserService;

@Controller
@RequestMapping("/shoppingCart")
public class CartController {
	@Autowired
	private UserService userService;
	
	@Autowired
	private CartItemService cartItemService;
	
	@Autowired
	private ArtworkService artworkService;
	
	@Autowired
	private CartItemListService cartItemListService;
	
	@RequestMapping("/cart")
	public String shoppingCart(Model model, Principal principal) {
		return "shoppingCart";
	}
//		User user = userService.findByUsername(principal.getName());
//		CartItemList shoppingCart = user.getCartItemList();
//		
//		List<CartItem> cartItemList = cartItemService.findByShoppingCart(shoppingCart);
//		
//		cartItemListService.updateShoppingCart(shoppingCart);
//		
//		model.addAttribute("cartItemList", cartItemList);
//		model.addAttribute("shoppingCart", shoppingCart);
//		
//		return "shoppingCart";
//	}

	@RequestMapping("/addItem")
	public String addItem(
			@ModelAttribute("artwork") Artwork artwork,
			@ModelAttribute("qty") String qty,
			Model model, Principal principal
			) {
		User user = userService.findByUsername(principal.getName());
		artwork = artworkService.findOne(artwork.getId());
		
		if (Integer.parseInt(qty) > artwork.getQuantity()) {
			model.addAttribute("notEnoughStock", true);
			return "forward:/bookDetail?id="+artwork.getId();
		}
		
		CartItem cartItem = cartItemService.addBookToCartItem(artwork, user, Integer.parseInt(qty));
		model.addAttribute("addBookSuccess", true);
		
		return "forward:/bookDetail?id="+artwork.getId();
	}
	
	@RequestMapping("/updateCartItem")
	public String updateShoppingCart(
			@ModelAttribute("id") Long cartItemId,
			@ModelAttribute("qty") int qty
			) {
		CartItem cartItem = cartItemService.findById(cartItemId);
		cartItem.setQty(qty);
		cartItemService.updateCartItem(cartItem);
		
		return "forward:/shoppingCart/cart";
	}
	
	@RequestMapping("/removeItem")
	public String removeItem(@RequestParam("id") Long id) {
		cartItemService.removeCartItem(cartItemService.findById(id));
		
		return "forward:/shoppingCart/cart";
	}
}
