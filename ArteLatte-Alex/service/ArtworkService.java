package com.ArteLatte.service;

import java.util.List;
import com.ArteLatte.domain.Artwork;

public interface ArtworkService {
	public List<Artwork> findAll();
	
	public Artwork findOne(Long id);
}
