package com.ArteLatte.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ArteLatte.domain.CartItem;
import com.ArteLatte.domain.CartItemList;
import com.ArteLatte.repository.CartItemListRepository;
import com.ArteLatte.service.CartItemService;
import com.bookstore.repository.ShoppingCartRepository;
import com.ArteLatte.service.CartItemListService;

@Service
public class CartItemListServiceImpl implements CartItemListService {

	@Autowired
	private CartItemService cartItemService;
	
	@Autowired
	private CartItemListRepository shoppingCartRepository;
	
	@Override
	public CartItemList updateShoppingCart(CartItemList shoppingCart) {
BigDecimal cartTotal = new BigDecimal(0);
		
		List<CartItem> cartItemList = cartItemService.findByShoppingCart(shoppingCart);
		
		for (CartItem cartItem : cartItemList) {
			if(cartItem.getArtwork().getQuantity() > 0) {
				cartItemService.updateCartItem(cartItem);
				cartTotal = cartTotal.add(cartItem.getSubtotal());
			}
		}
		
		shoppingCart.setGrandTotal(cartTotal);
		
		shoppingCartRepository.save(shoppingCart);
		
		return shoppingCart;
	}

}
