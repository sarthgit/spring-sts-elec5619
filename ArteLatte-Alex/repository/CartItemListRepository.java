package com.ArteLatte.repository;

import org.springframework.data.repository.CrudRepository;
import com.ArteLatte.domain.CartItemList;
public interface CartItemListRepository extends CrudRepository<CartItemList, Long> {

}
