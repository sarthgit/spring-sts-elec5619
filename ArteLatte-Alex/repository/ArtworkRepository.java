package com.ArteLatte.repository;

import org.springframework.data.repository.CrudRepository;

import com.ArteLatte.domain.Artwork;

public interface ArtworkRepository extends CrudRepository<Artwork, Long>{

}
